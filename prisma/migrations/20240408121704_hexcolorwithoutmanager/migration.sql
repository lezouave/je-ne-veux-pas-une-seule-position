/*
  Warnings:

  - You are about to drop the column `managerHexColor` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `managerHexContrastColor` on the `User` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "User" DROP COLUMN "managerHexColor",
DROP COLUMN "managerHexContrastColor",
ADD COLUMN     "hexColor" TEXT,
ADD COLUMN     "hexContrastColor" TEXT;
