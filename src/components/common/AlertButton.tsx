import {BellIcon} from "@heroicons/react/24/outline";
import React from "react";

const AlertButton = (props: { children: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | Promise<React.AwaitedReactNode> | null | undefined; }) => {
    return <button
        className="bg-white p-1 rounded-full text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
        <span className="sr-only">{props.children}</span>
        <BellIcon className="h-6 w-6" aria-hidden="true"/>
    </button>;
}

export default AlertButton;
