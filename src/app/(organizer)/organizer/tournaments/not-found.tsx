import React from 'react';


export default async function NotFoundPage(props: any) {
  return (
    <div>
      <h1>Pas de tournoi</h1>
      <p>Le tournoi a peut-être été supprimé ou vous n'avez pas les droits nécessaires</p>
    </div>
  );
}