import React from 'react';

import { getServerAuthSession } from '@/lib/auth';
import TournamentForm from '../[tournamentSlug]/edit/TournamentForm';

const TounamentNewPage = async () => {

    const session = await getServerAuthSession();

    return (
        <div>
            <TournamentForm />
        </div>
    );
}

export default TounamentNewPage;