'use server'

import { ActionError, userAction } from '@/app/safeAction';
import { TournamentSchema } from './tournament.schema';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const createTournamentAction = userAction(
    TournamentSchema, 
    async(input, context)  => {

        //verify if slug alread exists
        const slugExists = await prisma.tournament.count({
            where: {
                slug: input.slug,
            }
        });

        if(slugExists) {
            throw new ActionError('Slug already exists');
        }

      
        const tournament = await prisma.tournament.create({
            data: {
                ...input,
                organizerId: context.user.id,
            }
        });

        return tournament;
});

export const editTournamentAction = async (req: any, res: any) => {
    return res.json({ message: 'editTournamentAction' });
}

