'use client'

import { useForm } from 'react-hook-form';
import { zodResolver } from "@hookform/resolvers/zod";
import { FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage, Form } from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { Button } from '@/components/ui/button';
import { TournamentType, TournamentSchema } from './tournament.schema';
import { useMutation } from '@tanstack/react-query';
import { createTournamentAction } from './tournament.actions';
import { useRouter } from 'next/navigation';
import slugify from 'slugify';

export type TournamentFormProps = {
    defaultValues?: TournamentType;
}

const TournamentForm = (props: TournamentFormProps) => {


    const form = useForm<TournamentType>({
        resolver: zodResolver(TournamentSchema),
        defaultValues: props.defaultValues,
    })

    const router = useRouter();

    const isNew = !props.defaultValues;


    const mutation = useMutation({
        mutationFn: async (values: TournamentType) => {
            const { data, serverError } = await createTournamentAction(values);

            if (serverError || !data) {
                console.log('serverError', serverError);
            }
            router.push(`/organizer/tournaments/${data?.slug}/edit`);
        },
    });






    return (
        <>
            <h1>
                {isNew ? "Enregistrer un nouveau tournoi" : `Edition du tournoi ${props?.defaultValues?.name}`}
            </h1>

            <Form {...form} >
                <form onSubmit={form.handleSubmit(async (values) => {
                    console.log('new values', values);
                    const test = await mutation.mutateAsync(values);
                    console.log('test', test);
                })}>
                    <FormField
                        control={form.control}
                        name="name"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>Nom du tournoi</FormLabel>
                                <FormControl>
                                    <Input placeholder="nom" {...field} />
                                </FormControl>
                                <FormDescription>Saisir le nom du tournoi</FormDescription>
                                <FormMessage />
                            </FormItem>
                        )}
                    />

                    <FormField
                        control={form.control}
                        name="slug"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>Slug du tournoi</FormLabel>
                                <FormControl>
                                    <Input placeholder="slug" {...field}
                                        onChange={(event) => field.onChange(slugify(event.target.value, { lower: true, trim: false}))}    
                                    />  
                                </FormControl>
                                <FormDescription>Saisir le slug du tournoi</FormDescription>
                                <FormMessage />
                            </FormItem>
                        )}
                    />

                    <FormField
                        control={form.control}
                        name="location"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>Lieu</FormLabel>
                                <FormControl>
                                    <Input placeholder="lieu" {...field} />
                                </FormControl>
                                <FormDescription>Saisir le lieu du tournoi</FormDescription>
                                <FormMessage />
                            </FormItem>
                        )}
                    />

                    <FormField
                        control={form.control}
                        name="description"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>Description</FormLabel>
                                <FormControl>
                                    <Input placeholder="description" {...field} />
                                </FormControl>
                                <FormDescription>Saisir la description du tournoi</FormDescription>
                                <FormMessage />
                            </FormItem>
                        )}
                    />

                    <FormField
                        control={form.control}
                        name="startDate"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>Date de début</FormLabel>
                                <FormControl>
                                    <Input type="date"  {...field}
                                        value={
                                            field.value instanceof Date
                                                ? field.value.toISOString().split('T')[0]
                                                : field.value
                                        }

                                        onChange={(event) => field.onChange(new Date(event.target.value))}
                                    />
                                </FormControl>
                                <FormDescription>Saisir la date de début du tournoi</FormDescription>
                                <FormMessage />
                            </FormItem>
                        )}
                    />

                    <FormField
                        control={form.control}
                        name="endDate"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>Date de fin</FormLabel>
                                <FormControl>
                                    <Input type="date" {...field}
                                        value={
                                            field.value instanceof Date
                                                ? field.value.toISOString().split('T')[0]
                                                : field.value
                                        }
                                        onChange={(event) => field.onChange(new Date(event.target.value))}
                                    />
                                </FormControl>
                                <FormDescription>Saisir la date de fin du tournoi</FormDescription>
                                <FormMessage />
                            </FormItem>
                        )}
                    />

                    <FormField
                        control={form.control}
                        name="numberOfTeams"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>Nombre d'équipes</FormLabel>
                                <FormControl>
                                    <Input type="number" {...field}
                                        onChange={(event) => field.onChange(+event.target.value)}
                                    />
                                </FormControl>
                                <FormDescription>Saisir le nombre d'équipes</FormDescription>
                                <FormMessage />
                            </FormItem>
                        )}
                    />

                    <FormField
                        control={form.control}
                        name="registrationDeadline"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>Date limite d'inscription</FormLabel>
                                <FormControl>
                                    <Input type="date" {...field}
                                        value={
                                            field.value instanceof Date
                                                ? field.value.toISOString().split('T')[0]
                                                : field.value
                                        }
                                        onChange={(event) => field.onChange(new Date(event.target.value))}
                                    />
                                </FormControl>
                                <FormDescription>Saisir la date limite d'inscription</FormDescription>
                                <FormMessage />
                            </FormItem>
                        )}
                    />

                    <FormField
                        control={form.control}
                        name="registrationFee"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>Frais d'inscription</FormLabel>
                                <FormControl>
                                    <Input type="number" {...field}
                                        onChange={(event) => field.onChange(+event.target.value)}
                                    />
                                </FormControl>
                                <FormDescription>Saisir les frais d'inscription</FormDescription>
                                <FormMessage />
                            </FormItem>
                        )}
                    />

                    <FormField
                        control={form.control}
                        name="tournamentFormat"
                        render={({ field }) => (
                            <FormItem>
                                <FormLabel>Format du tournoi</FormLabel>
                                <FormControl>
                                    <Input placeholder="format" {...field} />
                                </FormControl>
                                <FormDescription>Saisir le format du tournoi</FormDescription>
                                <FormMessage />
                            </FormItem>
                        )}
                    />


                    <Button type="submit">Enregistrer le tournoi</Button>

                </form>
            </Form>
        </>

    );
}

export default TournamentForm;

