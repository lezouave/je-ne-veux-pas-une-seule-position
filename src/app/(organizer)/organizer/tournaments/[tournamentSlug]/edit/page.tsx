import { currentUser, getServerAuthSession } from '@/lib/auth';
import React from 'react';
import TournamentForm from './TournamentForm';
import prisma from '@/lib/prisma';
import { notFound } from 'next/navigation';


export default async function TournamentEditPage({params: {tournamentSlug} , ...props}: any) {

  const user = await currentUser();

  if(!tournamentSlug) {
    notFound();
  }

  const tournament = await prisma.tournament.findUnique({
    where: {
      slug: tournamentSlug,
      organizerId: user?.id,
    }
  });


  if(!tournament) {
    notFound();
  }

  return (
      <div>
          <TournamentForm defaultValues={tournament} />
      </div>
  );
}