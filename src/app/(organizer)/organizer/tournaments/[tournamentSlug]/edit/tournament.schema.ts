import { z } from 'zod';

export const TournamentSchema = z.object({
    name: z.string().min(1),
    slug: z.string().min(3).max(20).regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/),
    location: z.string().min(1),
    description: z.string().min(1),
    startDate: z.date(),
    endDate: z.date(),
    numberOfTeams: z.number(),
    registrationDeadline: z.date(),
    registrationFee: z.number(),
    tournamentFormat: z.string().min(1),
})

export type TournamentType = z.infer<typeof TournamentSchema>;