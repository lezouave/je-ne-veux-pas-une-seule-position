import { ReactNode } from 'react';
import Head from "next/head";
import LayoutNav from '@/components/LayoutNav';

const navigation = [
    { name: 'Ajouter un tournoi', href: '/organizer/tournaments/new' },
    { name: 'item', href: '/item' },
    { name: 'item', href: '/item' },
    { name: 'item', href: '/item' }
]


const Layout = ({ children }: { children: ReactNode }) => {
    return (
        <div className='max-w-full'>
            <Head>
                <title>Beach Volley Challenge</title>


            </Head>


            <div className="bg-white">
                <LayoutNav navigation={navigation} />

                <div className="py-10">
                    <header>
                        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                            <h1 className="text-3xl font-bold leading-tight text-gray-900">{ }</h1>
                        </div>
                    </header>
                    <main>
                        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                            {/* Replace with your content */}
                            <div className="px-4 py-8 sm:px-0">
                                {children}
                            </div>
                            {/* /End replace */}
                        </div>
                    </main>
                </div>
            </div>
        </div>
    )
}

export default Layout;
