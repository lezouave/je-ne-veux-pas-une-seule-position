import { Resend } from 'resend';
import HouseCodeTemplate from '../../../../emails/houseCode';

const resend = new Resend(process.env.RESEND_API_KEY);

export async function POST(request: Request) {
    const { searchParams } = new URL(request.url);
    const code = searchParams.get('code')?? '';

    try {
        const data = await resend.emails.send({
            from: 'MonAutreMaison',
            to: ['delivered@resend.dev'],
            subject: 'Le code de ma maison',
            react: HouseCodeTemplate( code ),
            text: '',
        });
        return Response.json(data);
    } catch (error) {
        return Response.json({ error });
    }
}
