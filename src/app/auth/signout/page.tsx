'use client';

import { signIn, signOut } from "next-auth/react";

export default function SignOutPage() {

  return (  
    <div className="flex flex-col items-center justify-center h-screen">
      <h1 className="text-4xl font-bold">Déconnexion</h1>
      <div className="mt-4">
        <button
          onClick={() => signOut()}
          className="px-4 py-2 bg-blue-500 text-white rounded-md"
        >
          Se déconnecter
        </button>
      </div>
    </div>
  );
}