import Link from 'next/link';
import React from 'react';

export default function VerifyRequestPage() {

  return (
    <div className="text-center">
      <div className="flex flex-col items-center justify-center border-2 p-12 m-12">
        <h1 className="text-4xl font-bold">Un mail vient de vous être envoyé pour vous connecter.</h1>


      </div>

      <Link href="/">Retour à l'accueil</Link>

    </div>

  );
}
