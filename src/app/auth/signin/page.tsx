export default function SignInPage() {

  return (
    <div className="flex flex-col items-center justify-center border-2 p-12">
      <div className="text-center">
        <h1 className="text-2xl font-bold">Envoyer un mail de connexion</h1>
        <div className="mt-4">
          <form action="/api/auth/signin/email" method="POST">
            <div className="flex flex-col gap-2">
            <input type="hidden" name="csrfToken" value="554ca4fca0024d54db19cdc27f93d73962b57e216d323cbd1bb324d02443e0df" />
            <label className="section-header" htmlFor="input-email-for-email-provider">Email</label>
            <input id="input-email-for-email-provider" type="email" name="email" placeholder="email@example.com" required={true} />
            <button id="submitButton" type="submit" className="px-4 py-2 border-2 border-blue-500 text-blue-500 rounded-md">Se connecter avec un Email</button>
            </div>       
          </form>
        </div>
      </div>
    </div>
  );
}
