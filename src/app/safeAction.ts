import { createSafeActionClient, DEFAULT_SERVER_ERROR } from "next-safe-action";
import { User } from "@prisma/client";
import { getServerAuthSession } from "@/lib/auth";


export class ActionError extends Error {
  constructor(message: string) {
    super(message);
    this.name = "ActionError";
  }
}

const handleReturnedServerError = (error: any) => {

  if (error instanceof ActionError) {
    return error.message;
  }

  return DEFAULT_SERVER_ERROR;
};

export const action = createSafeActionClient({
  handleReturnedServerError,
});

export const userAction = createSafeActionClient({

  handleServerErrorLog(error) {
    console.error("handleServerErrorLog", error);
  },
  middleware: async () => {
    const session = await getServerAuthSession();
    console.log("session", session);
    if (!session?.user?.email) {
      throw new ActionError("You must be logged in to perform this action");
    }

    return {user: session?.user as User};
  },
});
