import "../../styles/globals.css";
import "react-big-calendar/lib/css/react-big-calendar.css";
import MessageProvider from "../store/message";
import UserProvider from "@/store/user";
import GoogleAnalytics from "@/components/GoogleAnalytics";
import Providers from "@/components/Providers";
import {
  useQuery,
  useMutation,
  useQueryClient,
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query'


function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="fr">
      <head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </head>
      <GoogleAnalytics />
      <body className="w-full">
          <Providers>
            <MessageProvider>
              <UserProvider>
                  {children}
              </UserProvider>
            </MessageProvider>
          </Providers>
      </body>
    </html>
  );
}

export default RootLayout;
