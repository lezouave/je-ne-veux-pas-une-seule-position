import { PrismaClient } from '@prisma/client';

let prisma: PrismaClient;

declare const global: any; // Add this line to explicitly define the type of global

if (process.env.NODE_ENV === 'production') {
  prisma = new PrismaClient();
} else {
  if (!global.prisma) {
    global.prisma = new PrismaClient();
  }
  prisma = global.prisma;
}

export default prisma;