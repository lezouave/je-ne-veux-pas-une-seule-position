import NextAuth, { NextAuthOptions, User, getServerSession } from "next-auth";
import { PrismaAdapter } from "@auth/prisma-adapter";
import { PrismaClient } from "@prisma/client";
import EmailProvider from "next-auth/providers/email";
import { sendVerificationRequest } from "./sendVerificationRequest";
import { AdapterUser } from "next-auth/adapters";
import { get } from "http";

const prisma = new PrismaClient();


export const authOptions: NextAuthOptions = {
  adapter: PrismaAdapter(prisma) as any,
  providers: [
    EmailProvider({
      server: {
        host: process.env.EMAIL_SERVER_HOST,
        port: parseInt(process.env.EMAIL_SERVER_PORT ?? ""),
        auth: {
          user: process.env.EMAIL_SERVER_USER,
          pass: process.env.RESEND_API_KEY,
        },
      },
      from: process.env.EMAIL_FROM,
      sendVerificationRequest({
        identifier: email,
        url,
        provider: { server, from },
      }) {
        sendVerificationRequest({
          identifier: email,
          url,
          provider: { server, from },
        });
      },
    }),
  ],

  pages: {
    signIn: "/auth/signin",
    signOut: "/auth/signout",
    error: "/auth/error",
    verifyRequest: "/auth/verify-request",
  },

  callbacks: {
    async signIn({ user}: { user: User | AdapterUser;}) {
      const userInDb = await prisma.user.findUnique({
        where: { email: user?.email ?? undefined },
      });

      if (!userInDb) {

        const emailVerified = "emailVerified" in user ? user.emailVerified : null;
      
        try {
          await prisma.user.create({
            data: {
              id: user.id,
              name: user.name,
              email: user.email,
              emailVerified: emailVerified,
              image: user.image,
            },
          });
        } catch (error) {
          console.log("error", error);
        }

        return true;
      }

      return true;
    },

    async redirect({ url, baseUrl }) {
      return baseUrl;
    },

    async session({ session, token, user }) {
      return {
        ...session,
        user: {
          ...session.user,
          id: user.id,
        },
      };
    },
  },
  secret: process.env.NEXTAUTH_SECRET,
};

export const getServerAuthSession = () => getServerSession(authOptions);

export const currentUser = async() => {
  const session = await getServerAuthSession();
  if(!session || !session?.user) {
    throw new Error("User not found");
  }
  return session?.user as User | undefined;
}