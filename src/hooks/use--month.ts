import moment from "moment/moment";
import {useState} from "react";

const useMonth = () => {
    const startMonthBefore = moment(new Date()).subtract(1, 'months').startOf('month').format('YYYY-MM-DD');
    const [startMonth, setStartMonth] = useState(startMonthBefore);
    const endMonthBefore = moment(new Date()).subtract(1, 'months').endOf('month').format('YYYY-MM-DD');
    const [endMonth, setEndMonth] = useState(endMonthBefore);
    
    const monthChange = (monthTarget: { target: string; }) => {
        if (monthTarget.target === 'before') {
            const startMth = moment(startMonth).subtract(1, 'months').startOf('month').format('YYYY-MM-DD')
            setStartMonth(startMth);
            const endMth = moment(endMonth).subtract(1, 'months').endOf('month').format('YYYY-MM-DD');
            setEndMonth(endMth);
        } else if (monthTarget.target === 'after') {
            const startMth = moment(startMonth).add(1, 'months').startOf('month').format('YYYY-MM-DD');
            setStartMonth(startMth);
            const endMth = moment(endMonth).add(1, 'months').endOf('month').format('YYYY-MM-DD');
            setEndMonth(endMth);
        }
    };
    
    return {
        startMonth,
        endMonth,
        monthChange
    }
};

export default useMonth;
