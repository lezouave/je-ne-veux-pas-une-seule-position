'use client'

import { User } from "@prisma/client";
import { createContext, useState } from "react";
 
const initCtx = {
    user: {
        id: '',
        name: '',
        email: '',
        emailVerified: '',
        image: '',
    },

    setUser: (user: {
        id: string;
        name: string;
        email: string;
        emailVerified: string;
        image: string;
    }) => {}
}

export const UserContext = createContext(initCtx);

import { ReactNode } from "react";

const UserProvider = ({ children }: { children: ReactNode }) => {
    const [user, setUser] = useState({
        id: '',
        name: '',
        email: '',
        emailVerified: '',
        image: '',
    });
 
    const ctx = {
        user: user as {
            id: string;
            name: string;
            email: string;
            emailVerified: string;
            image: string;
        },
        setUser: setUser as (user: {
            id: string;
            name: string;
            email: string;
            emailVerified: string;
            image: string;
        }) => void
    };

    return <UserContext.Provider value={ctx}>{children}</UserContext.Provider>
}

export default UserProvider;
